export default [
  {
    name: 'Moon Shadow',
    breed: 'Saint Bernard',
    gender: 'female',
    age: 12,
    color: 'brown',
    weight: 85,
    location: 'Portland',
    notes: 'dads first dog'
  },
  {
    name: 'Missy',
    breed: 'Akita',
    gender: 'female',
    age: 10,
    color: 'white',
    weight: 75,
    location: 'Milwaukie',
    notes: 'parents dog'
  },
  {
    name: 'Barley',
    breed: 'mut',
    gender: 'female',
    age: 7,
    color: 'red',
    weight: 55,
    location: 'Eugene',
    notes: 'miss her'
  },
  {
    name: 'Porter',
    breed: 'German Shepard',
    gender: 'male',
    age: 8,
    color: 'black/brown',
    weight: 65,
    location: 'Chicago',
    notes: 'good boy'
  }
]
