export default [
  {
    name: 'Pepper',
    breed: 'Maine Coon',
    gender: 'female',
    age: 7,
    color: 'black/white',
    weight: 34,
    location: 'Portland',
    notes: 'something silly'
  },
  {
    name: 'Sugar',
    breed: 'Siamese',
    gender: 'male',
    age: 8,
    color: 'white',
    weight: 37,
    location: 'Portland',
    notes: 'something else silly'
  },
  {
    name: 'Figaro',
    breed: 'Persian',
    gender: 'male',
    age: 10,
    color: 'black/white',
    weight: 34,
    location: 'Portland',
    notes: 'something silly'
  }
]
