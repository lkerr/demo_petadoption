export default {
  animalsCount: ( state ) => {
    return state.cats.length + state.dogs.length
  },
  getOldPets: ( state ) => {
    return state.pets.filter((pets) => {
      return pets.age >= 10
    }).length
  }
}
